﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class target : MonoBehaviour{

    public Rigidbody Ball;

    private Vector3 velocity;
    private Vector3 UpPos;
    private Vector3 downPos;

    public int intens = 1;
    
    public bool hit = false;
    public bool goUp = false;
    //------------------------
    private Color firsColor;// = new Color(30,0,30);
    private Color lastColor;
    private float chaceColorDuration;
    private Material color;

    private float lerp;
    private Renderer rend;
    //---------------------------

    private void Start()
    {

        rend = GetComponent<Renderer>();
        UpPos = this.transform.localPosition;
        downPos = UpPos;
        downPos.y = UpPos.y - 2;
    }

    // Update is called once per frame
    void Update()
    {
        velocity = Ball.gameObject.GetComponent<Rigidbody>().velocity;
        //WabeColor();
        MoveDown();
        if (goUp == true)
            MoveUp();

    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.rigidbody.tag == "Ball")
        {
            Ball.GetComponent<Rigidbody>().AddForce((velocity * -1) * intens, ForceMode.VelocityChange);
            hit = true;
        }
    }

    void WabeColor()
    {
        lerp = Mathf.PingPong(Time.deltaTime,chaceColorDuration) / chaceColorDuration;

        rend.material.color = Color.Lerp(firsColor, lastColor, lerp);
    }

    void MoveDown()
    {
        if (hit && downPos.y <= this.transform.localPosition.y)
        {
            this.transform.Translate((Vector3.down * 5) * Time.deltaTime);
        }
    }

    void MoveUp()
    {
        hit = false;
        if (UpPos.y >= this.transform.localPosition.y)
        {
            this.transform.Translate((Vector3.up * 5) * Time.deltaTime);
        }else{
            goUp = false;
            this.transform.localPosition = UpPos;
        }
    }

}
