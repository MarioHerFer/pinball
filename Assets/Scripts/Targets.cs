﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targets : MonoBehaviour
{
    [SerializeField] GameObject DeadBall;
    [SerializeField] GameObject[] target;

    [SerializeField] bool[] Hit;

    [SerializeField] float maxCountUp = 0.5f;
    private float countUP = 0;
    
    bool Multiply = false;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Hit[0] = target[0].GetComponent<target>().hit;
        Hit[1] = target[1].GetComponent<target>().hit;
        Hit[2] = target[2].GetComponent<target>().hit;

        if (Hit[0] == Hit[1] && Hit[1] == Hit[2] && Hit[0] == true)
        {
            
            if (countUP > maxCountUp)
            {
                for (int i = 0; i < target.Length; i++)
                {
                    target[i].GetComponent<target>().goUp = true;
                }
            }
            countUP += Time.deltaTime;
            if (!Multiply)
            {
                DeadBall.gameObject.GetComponent<DeadBall>().multiply += 3;
                Multiply = true;
            }
        }
        else
        {
            countUP = 0;
            Multiply = false;
        }

        
    }

}


