﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_cs : MonoBehaviour {

    public GameObject Ball;

    public Vector3 StartPos;

    public bool Reset;

    // Use this for initialization
    void Start()
    {
        StartPos = Ball.transform.position;
        Reset = false;
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Reset){
            Ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
            Ball.transform.position = StartPos;
            Reset = false;
        }
       
    }
    

    
}
