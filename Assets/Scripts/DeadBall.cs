﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadBall : MonoBehaviour {

    [SerializeField] GameObject Ball;
    public int nBalls = 0;

    public TextMesh text;
    public TextMesh scoreText;
    public int score = 0;
    public int multiply = 0;

    // Update is called once per frame
    void Update () {
        //Insert Coin
        if (nBalls == 0 && Input.GetKey(KeyCode.F))
        {
        nBalls = 3;
        score = 0;
        
        Ball.gameObject.SetActive(true);
            Ball.transform.position = Ball.GetComponent<Ball_cs>().StartPos;
        }
        

        if (text)
        {
            text.text = nBalls.ToString();
            if (multiply < 1)
            {
                scoreText.text = "x1" + "\n \n" + score.ToString();
            }else{
                scoreText.text = "x" + multiply.ToString() + "\n \n" + score.ToString();
            }
            if (nBalls == 0)
            {
                scoreText.text = "x" + multiply.ToString() + "\n \n" + score.ToString() + "\n \n" + "press f to reset balls";
            }
        }

    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.rigidbody.tag == "Ball"){
            
         if (nBalls >0){
                Ball.GetComponent<Ball_cs>().Reset = true;
                nBalls--;
            }
            if (nBalls == 0)
            {
                Ball.gameObject.SetActive(false);
            }
    
         

        }
    }
    
    public void SumScore(int pls)
    {
        if (multiply < 1)
        {
            score = score + (pls * 1);
        }
        else
        {
            score = score + (pls * multiply);
        }
        
    }
}
