﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomper_cs : MonoBehaviour {

    [SerializeField] Rigidbody Ball;
    [SerializeField] GameObject DeadBall;
    private Vector3 velocity;
    [SerializeField] int intens = 1;

    // Update is called once per frame
    void Update()
    {
        velocity = Ball.gameObject.GetComponent<Rigidbody>().velocity;
    }

    private void OnCollisionEnter(Collision other){

        if (other.rigidbody.tag == "Ball")
        {
            Ball.GetComponent<Rigidbody>().AddForce((velocity * -1) * intens, ForceMode.Impulse);
            DeadBall.GetComponent<DeadBall>().SumScore(100);
        }
    }
}
